# Yggdrasill/Ops_Tools #

Yggdrasill.ca bespoke operational tools.

## Scripts ##

* kafkatopics

### kafkatopics ###

    Usage: kafkatopics [OPTIONS]

    Options:
    --action TEXT         Script function to invoke. Options are
                          create/update/delete.  [required]

    --brokers TEXT        Kafka broker host list.  [default: localhost:9092]
    --topiclist FILENAME  Yaml configuration file.  [required]
    --validate            Validate input(Dry run, no actual changes made.)
    --help                Show this message and exit.


* Configuration
  - etc/kafka-config.sample: Sample configuration file for kafka topic list.

* Dependencies
  - kafka-python >= 1.4.7
